# Contrl IRC bot #

## Ability to: ##
- Connect to a CA or self-signed IRC server, through Tor
- Use TLS
- Log all actions/messages to a file, separated by day
- Authenticate with Nickserv or SASL
- ~~Can be issued commands~~ BEING REWORKED, ETA: IDK


# Current Release: #
- 14.12.13 ( 2014, December, 13th )


# WONTFIX: #
- None right now


# Issues: #
- Are you using the most up-to-date version of the bot?
- Is your issue unique?
- Have you checked the issues to ensure this hasn't been brought up by someone else?

### If you've checked and made sure of all of these then please post a new issue with: ###
- #### The bot version you're using ####
- #### A full traceback ####
- #### What your issue is, and how we can reproduce it ####
- #### What operating system you're getting this issue on ####


# I'M HAVING PROBLEMS!: #
- 1 Why am I getting


```
#!python
sent = self._tlsConnection.send(toSend)
OpenSSL.SSL.Error: [('SSL routines', 'SSL23_GET_SERVER_HELLO', 'unknown protocol')]
```

Because you're trying an insecure port but have opted to use TLS.

- 2 Why am I getting
```
#!python
main function encountered error
Traceback (most recent call last):
Failure: twisted.internet.error.ConnectionDone: Connection was closed cleanly.
```

Because you're trying a secure port but have opted to not use TLS.


# It's Implied That: #
- **The server admin has prevented registration of the usernames "nickserv" and "chanserv"**
- You're using Pidgin, if you have a self-signed server you're connecting to


# Warning! Dragons Ahead!: #
This bot has only been tested on and proved to work with the following:
CrunchBang, Debian, and Ubuntu

If your operating system does not work with the bot for one reason or
another, and you'd like help to test and get it working on your
operating system, please file an issue with traceback, version of OS,
and which version of the bot you were using. This way we can help you
and ensure more users can use this program!


# Required: #
### Debian/Ubuntu: ###
- sudo apt-get install build-essential python-dev python-pip python-openssl
- pip install --user idna service_identity twisted txsocksx
### Windows Vista/7/8: ###
- ~~pywin32 ( http://sourceforge.net/projects/pywin32/files/pywin32/ )~~
- ~~pip ( https://pip.pypa.io/en/latest/installing.html)~~
- ~~pip install idna service_identity twisted txsocksx~~
- Check #19

# WTF Is With These Numbers?: #
- These are used in defs.py
- 12 + 0 + 4 = 16 NO TLS / NO SASL
- 12 + 2 + 4 = 18 NO TLS / YES SASL
- 24 + 0 + 4 = 28 YES TLS / NO SASL / NO SELF-SIGNED
- 24 + 2 + 4 = 30 YES TLS / YES SASL / NO SELF-SIGNED
- 24 + 0 + 8 = 32 YES TLS / NO SASL / YES SELF-SIGNED
- 24 + 2 + 8 = 34 YES TLS / YES SASL / YES SELF-SIGNED


# CTCP TIME: #
In "CTCP TIME" there is 
```
#!python
time.asctime(time.gmtime(time.time()))
```
and this is where we set gmt time by default.

Check out [https://docs.python.org/2/library/time.html](https://docs.python.org/2/library/time.html)
for information on what to set for a specific time.


# CA Setup: #

Edit the following CHANGE_ME
lines in order to run the bot:
( ^ at the end denotes something that can be changed to '' to show it's not in use )

1. tor_port ( Tor Socks Port ( 9050 by default for daemon, 9150 if using Browser Bundle )) 
2. server_password ( Server-wide password ) ^
[ http://askubuntu.com/questions/181111/how-can-i-set-ircd-hybrid-server-password ]
3. nickserv_pass ( NickServ password ) ^ but **DANGEROUS** 
4. channel_ ( Channel to join ( must start with # - like #example ))
5. server_ ( Network to join ( irc.example.net ))
6. port_ ( Port number ( 6697 ))
7. common_name ( Hostname for the certificate
( Go to Pidgin's Buddy List -> Tools -> Certificates ->
Get Info -> "Common name" for what to put here.
You can also go to the bottom of this page and view
the "Common Name" section.))
8. cert_ ( Where to find the certificate ) ^
9. username_ ( Bot nickname, change it to whatever you want )
10. sourceURL ( Source code for this bot ) ^
11. key ( Channel room password ) ^
12. finger_var ( Owner information ) ^
13. versionNa_var ( What is the name of this bot? ) ^
14. versionNu_var ( Version of this bot ) ^
15. versionEn_var ( What environment? Linux, BSD, etc ) ^


# Self-Signed Setup: #
Edit the following CHANGE_ME
lines in order to run the bot:
( ^ denotes something that can be changed to '' to show it's not in use )

1. tor_port ( Tor Socks Port ( 9050 by default for daemon, 9150 if using Browser Bundle )) 
2. server_password ( Server-wide password ) ^
[ http://askubuntu.com/questions/181111/how-can-i-set-ircd-hybrid-server-password ]
3. nickserv_pass ( NickServ password ) ^ but **DANGEROUS** 
4. channel_ ( Channel to join ( must start with # - like #example ))
5. server_ ( Network to join ( irc.example.net ))
6. port_ ( Port number ( 6697 ))
7. common_name ( Hostname for the certificate
( Go to Pidgin's Buddy List -> Tools -> Certificates ->
Get Info -> "Common name" for what to put here.
You can also go to the bottom of this page and view
the "Common Name" section.))
8. cert_ ( Where to find the certificate )
Set to something like:
/home/username/.purple/certificates/x509/tls_peers/irc.example.net
9. username_ ( Bot nickname, change it to whatever you want )
10. sourceURL ( Source code for this bot ) ^
11. key ( Channel room password ) ^
12. finger_var ( Owner information ) ^
13. versionNa_var ( What is the name of this bot? ) ^
14. versionNu_var ( Version of this bot ) ^
15. versionEn_var ( What environment? Linux, BSD, etc ) ^


# NON-TLS Setup: #
Edit the following CHANGE_ME
lines in order to run the bot:
( ^ denotes something that can be changed to '' to show it's not in use )

1. tor_port ( Tor Socks Port ( 9050 by default for daemon, 9150 if using Browser Bundle )) 
2. server_password ( Server-wide password ) ^
[ http://askubuntu.com/questions/181111/how-can-i-set-ircd-hybrid-server-password ]
3. nickserv_pass ( NickServ password ) ^ but **DANGEROUS** 
4. channel_ ( Channel to join ( must start with # - like #example ))
5. server_ ( Network to join ( irc.example.net ))
6. port_ ( Port number ( 6667 ))
7. common_name ( Hostname for the certificate ) ^
8. cert_ ( Where to find the certificate ) ^
9. username_ ( Bot nickname, change it to whatever you want )
10. sourceURL ( Source code for this bot ) ^
11. key ( Channel room password ) ^
12. finger_var ( Owner information ) ^
13. versionNa_var ( What is the name of this bot? ) ^
14. versionNu_var ( Version of this bot ) ^
15. versionEn_var ( What environment? Linux, BSD, etc ) ^


# Common Name: #
- FREENODE: chat.freenode.net
- OFTC: irc.oftc.net


**Have fun!**