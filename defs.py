#!/usr/bin/env python2.7
#
import sys
import time

sav = 'settings.ini'

# Self-signed / CA / SASL questions
def question():
    try:
        tls_q = str(raw_input('Would you like to use '
        + 'TLS? (Y)ES/(N)O: '))
    except ValueError:
        print 'Input was not Y, YES, N, or NO, stopping program.'
        sys.exit()
    if (tls_q.upper() == 'Y' or tls_q.upper() == 'YES'
    or tls_q.upper() == 'N' or tls_q.upper() == 'NO'):
        if tls_q.upper() == 'N' or tls_q.upper() == 'NO':
            tls_var = 16
            try:
                sasl_q = str(raw_input('Would you like to use '
                + 'SASL authentication? (Y)ES/(N)O: '))
            except ValueError:
                print 'Input was not Y, YES, N, or NO, stopping program.'
                sys.exit()
            if (sasl_q.upper() == 'Y' or sasl_q.upper() == 'YES'
            or sasl_q.upper() == 'N' or sasl_q.upper() == 'NO'):
                if sasl_q.upper() == 'N' or sasl_q.upper() == 'NO':
                    sasl_var = 0
                else:
                    sasl_var = 2
                with open(sav, 'w') as sav1:
                    sav1.write(str(tls_var + sasl_var))
                    sav2 = tls_var + sasl_var
        else:
            tls_var = 24
            try:
                sasl_q = str(raw_input('Would you like to use '
                + 'SASL authentication? (Y)ES/(N)O: '))
            except ValueError:
                print 'Input was not Y, YES, N, or NO, stopping program.'
                sys.exit()
            if (sasl_q.upper() == 'Y' or sasl_q.upper() == 'YES'
            or sasl_q.upper() == 'N' or sasl_q.upper() == 'NO'):
                if sasl_q.upper() == 'N' or sasl_q.upper() == 'NO':
                    sasl_var = 0
                else:
                    sasl_var = 2
                try:
                    cert_q = str(raw_input('Would you like to connect '
                    + 'to a self-signed server? (Y)ES/(N)O: '))
                except ValueError:
                    print 'Input was not Y, YES, N, or NO, stopping program.'
                    sys.exit()
                if (cert_q.upper() == 'Y' or cert_q.upper() == 'YES'
                or cert_q.upper() == 'N' or cert_q.upper() == 'NO'):
                    if cert_q.upper() == 'N' or cert_q.upper() == 'NO':
                        cert_var = 4
                    else:
                        cert_var = 8
                    with open(sav, 'w') as sav1:
                        sav1.write(str(tls_var + sasl_var + cert_var))
                        sav2 = tls_var + sasl_var + cert_var
                else:
                    print 'Input was not Y, YES, N, or NO, stopping program.'
                    sys.exit()
            else:
                print 'Input was not Y, YES, N, or NO, stopping program.'
                sys.exit()
    else:
        print 'Input was not Y, YES, N, or NO, stopping program.'
        sys.exit()

def wipe_q():
    try:
        wipe_qst = str(raw_input('Would you like to wipe settings? '
        + '(Y)ES/(N)O: '))
    except ValueError:
        print 'Input was not Y, YES, N, or NO, stopping program.'
        sys.exit()
    if (wipe_qst.upper() == 'Y' or wipe_qst.upper() == 'YES'
    or wipe_qst.upper() == 'N' or wipe_qst.upper() == 'NO'):
        with open(sav, 'r') as sav1:
            sav2 = sav1.readline()
        if wipe_qst.upper() == 'Y' or wipe_qst.upper() == 'YES':
            with open(sav, 'w') as sav1:
                sav1.truncate(0)
            print 'Settings were wiped. Please run the program again.'
            sys.exit()
        else:
            pass

# Time used for printing to screen
def print_message(message_):
    return time.strftime('[%H:%M:%S on %B %d, %Y] ' + message_)

# Writing to file
def log_file(message_):
    with open(time.strftime('%B %d, %Y.txt'), 'a') as log1:
        log1.write(time.strftime('[%H:%M:%S] ' + message_ + '\n'))